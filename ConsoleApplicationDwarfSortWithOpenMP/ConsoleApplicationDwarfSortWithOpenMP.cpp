﻿#include <iostream>
#include <vector>
#include <omp.h>
using namespace std;

const long N = 10000;

std::vector<long> ArrM;
std::vector<long> ArrP;

std::vector<long> readArr()
{
    std::vector<long> localArr;
    for (int i = 0; i < N; i++)
        localArr.push_back( rand() % 200 - 100);
    return localArr;
}

void DivideArr(std::vector<long> localArr)
{
    ArrM.clear();
    ArrP.clear();
    for (size_t i = 0; i < localArr.size(); i++)
    {
        if (localArr[i]<=0)
        {
            ArrM.push_back(localArr[i]);
        }
        else
        {
            ArrP.push_back(localArr[i]);
        }

    }
    
}

std::vector<long> mergeArr()
{
    ArrM.insert(ArrM.end(),ArrP.begin(),ArrP.end());

    return ArrM;
}

std::vector<long> gnomeSort(std::vector<long> localArr)
{
    int index = 0;

    while (index < localArr.size()) {
        if (index == 0)
            index++;
        if (localArr[index] >= localArr[index - 1])
            index++;
        else {
            int temp = 0;
            temp = localArr[index];
            localArr[index] = localArr[index - 1];
            localArr[index-1] = temp;
            index--;
        }
    }
    return localArr;
}


void printArray(std::vector<long> localArr)
{
    cout << "Array : ";
    for (int i = 0; i < localArr.size(); i++)
        cout << localArr[i] << " ";
    cout << "\n";
}


int main()
{
    std::vector<long> arr;
    double  oneTime;
    for (size_t i = 1; i <= 2; i++)
    {
        arr = readArr();
        double time = omp_get_wtime();
        if (i==2)
        {
            DivideArr(arr);
            
            #pragma omp parallel sections num_threads(2)
            {
                
                #pragma omp section
                {
                    ArrM = gnomeSort(ArrM);
                }
                #pragma omp section
                {
                    ArrP = gnomeSort(ArrP);
                }


            }
           /* printArray(ArrM);
            printArray(ArrP);*/
            arr = mergeArr();
           // printArray(arr);
        }
        else
        {
            arr = gnomeSort(arr);
           // printArray(arr);
        }

        double time1 = omp_get_wtime();
        if (i == 1)
        {
            oneTime = time1 - time;
        }
        double thisTime = time1 - time;
        printf("threads = %d\ttime = %f acc = %f\n", i, thisTime, oneTime / thisTime);
       
       


    }
    return (0);
}